package com.example.pd_2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

public class Main2Activity extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        // Objekti prieks vertibu saglabasanas un nolasisanas:
        final SharedPreferences pref = getApplicationContext().getSharedPreferences("SavedList", 0);
        final SharedPreferences.Editor editor = pref.edit();

        // 2. Aktivitates Poga:
        Button button_activity_2 = findViewById(R.id.button_activity_2);
        button_activity_2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                finish();
            }
        });

        // Nolasisanas Poga:
        Button button_read = findViewById(R.id.button_save4);
        button_read.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                final EditText mEdit = findViewById(R.id.text_Input4);

                Toast.makeText(Main2Activity.this, getString(R.string.message_text_read) + " " + pref.getString("InputValue", null), Toast.LENGTH_LONG).show();
                mEdit.setText(pref.getString("InputValue", null));
            }
        });
    }
}