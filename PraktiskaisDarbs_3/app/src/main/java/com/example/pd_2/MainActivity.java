package com.example.pd_2;

import androidx.appcompat.app.AppCompatActivity;

import android.widget.Button;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import android.widget.EditText;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.view.inputmethod.InputMethodManager;

public class MainActivity extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Objekti prieks vertibu saglabasanas un nolasisanas:
        final SharedPreferences pref = getApplicationContext().getSharedPreferences("SavedList", 0);
        final Editor editor = pref.edit();

        // 2. Aktivitates Poga:
        Button button_activity_1 = findViewById(R.id.button_activity_1);
        button_activity_1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent1 = new Intent(MainActivity.this, Main2Activity.class);
                startActivity(intent1);
            }
        });

        // Saglabasanas Poga:
        Button button_save = findViewById(R.id.button_save);
        button_save.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                final EditText mEdit = findViewById(R.id.text_Input);

                if (mEdit.getText().toString().trim().length() > 0)
                {
                    Toast.makeText(MainActivity.this, getString(R.string.message_text_saved) + " " + mEdit.getText().toString(), Toast.LENGTH_LONG).show();
                    editor.putString("InputValue", mEdit.getText().toString());
                    editor.commit();
                }
                else
                    Toast.makeText(MainActivity.this, getString(R.string.message_nothing_to_save), Toast.LENGTH_SHORT).show();

                try
                {
                    InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                }
                catch (Exception e)
                {
                    Toast.makeText(MainActivity.this, getString(R.string.message_error), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}